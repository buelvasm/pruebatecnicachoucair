# Autor: Manuel
  @stories
  Feature: Academy Choucair
    As user, I want to learn how to automate in screamplay at the Choucair Academy with the automation course
  @scenario1
  Scenario: Search for a automation course
    Given tha Manuel wants to learn automation at the academy choucair
    |strUser | strPassword |
    |1007183249  | Choucair2021*|
    When he search for the course  on then choucair academy platform
    |strCourse     |
    |Recursos Automatizacion Bancolombia              |
    Then he finds the course called resources
    |strCourse   |
    |Recursos Automatizacion Bancolombia            |